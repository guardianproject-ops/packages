name: Ops Packages

description: |-
  Guardian Project distribution of apps for ops.

introduction: |-

  Use this repo to easily install releases of apps used for ops and dev.

  * **Make Based Installer.** This installer works regardless of your OS and
  distribution. It downloads packages directly from their GitHub/GitLab source
  repos, and installs them to your `INSTALL_PATH`. 

  See examples below for usage.

  **Is one of our packages out of date?**

  Open up an [issue](https://gitlab.com/guardianproject-ops/packages/issues) or submit a PR (*preferred*). We'll review quickly!

  This project is inspired by [CloudPosse's packages](https://github.com/cloudposse/packages),
  massive props to them. If you're looking for quality DevOps tooling, [check them
  out](https://cpco.io/sweetops).

# License of this project
license: "APACHE2"

# Canonical GitHub repo 
gitlab_repo: guardianproject-ops/packages

# Badges to display
# Badges to display
badges:
  - name: "Build Status"
    image: "https://gitlab.com/guardianproject-ops/packages/badges/master/pipeline.svg"
    url: "https://gitlab.com/guardianproject-ops/packages/-/commits/master"

related:
  - name: "build-harness"
    description: "Collection of Makefiles to build all the things."
    url: "https://gitlab.com/guardianproject-ops/build-harness"

# Other files to include in this README from the project folder
include:
  - "docs/targets.md"

usage: |-

  ### Makefile Interface

  The `Makefile` interface works on OSX and Linux. It's a great way to
  distribute binaries in an OS-agnostic way which does not depend on a package
  manager (e.g. no `brew` or `apt-get`).

  This method is ideal for local development environments (which is how we use
  it) where you need the dependencies installed natively for your
  OS/architecture, such as installing a package on OSX.

  **Side note**: we would much prefer to depend on system package managers.
  They are more reliable, more secure and totally trusted. However,
  unfortuantely the majority of the tools we rely on are not in system package
  managers. If they are, then they are invariably so out of date as to make
  them useless.


  See all available packages:
  ```
  make -C install help
  ```

  Install everything...
  ```
  make -C install all
  ```

  Install specific packages:
  ```
  make -C install terraform
  ```

  Install to a specific folder:
  ```
  make -C install terraform INSTALL_PATH=/usr/bin
  ```

  Uninstall a specific package
  ```
  make -C uninstall yq
  ```

examples: |-

  ### Makefile Inclusion

  Sometimes it's necessary to install some binary dependencies when building
  projects. For example, we frequently rely on `gomplate` or `terraform`.

  Here's a stub you can include into a `Makefile` to make it easier to install
  binary dependencies.

  ```
  export PACKAGES_VERSION ?= master
  export PACKAGES_PATH ?= packages/
  export INSTALL_PATH ?= $(PACKAGES_PATH)/bin

  ## Install packages
  packages/install:
          @if [ ! -d $(PACKAGES_PATH) ]; then \
            echo "Installing packages $(PACKAGES_VERSION)..."; \
            rm -rf $(PACKAGES_PATH); \
            git clone --depth=1 -b $(PACKAGES_VERSION) https://gitlab.com/guardianproject-ops/packages.git $(PACKAGES_PATH); \
            rm -rf $(PACKAGES_PATH)/.git; \
          fi

  ## Install package (e.g. terraform, terraform-docs)
  packages/install/%: packages/install
          @make -C $(PACKAGES_PATH)/install $(subst packages/install/,,$@)

  ## Uninstall package (e.g. terraform, terraform-docs)
  packages/uninstall/%:
          @make -C $(PACKAGES_PATH)/uninstall $(subst packages/uninstall/,,$@)
  ```

  ### Contributing Additional Packages
  In addition to following the Contributing section, the following steps can be used to add new packages for review (via a PR).
  1. Clone an existing, similar, package within the vendors directory. Name the new folder with the same name as the binary package being installed.
  2. At a minimum, update the `VERSION`, `DESCRIPTION`, and `Makefile` to reflect the binary being installed. Ensure that a test task exist in the package Makefile.
  3. Test the install and ensure that it downloads and runs as expected (`make -C install <your_package> INSTALL_PATH=/tmp`)
  4. Test the apk build (see below)
  5. Update the `README.md` (`make init readme/deps readme`)


# Copyrights
copyrights:
  - name: "The Guardian Project"
    url: "https://guardianproject.info"
    year: "2020"
  - name: "Center for Digital Resilience"
    url: "https://digiresilience.org"
    year: "2020"
  - name: "Cloud Posse, LLC"
    url: "https://cloudposse.com"
    year: "2016"

funded_by_cdr: true
# Contributors to this project
contributors:
  - name: "Abel Luck"
    gitlab: "abelxluck"
