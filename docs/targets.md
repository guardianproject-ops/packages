## Makefile Targets
```
ansible                   2.9.6      Ansible is a radically simple IT automation platform that makes your applications and systems easier to deploy. Avoid writing scripts or custom code to deploy and update your applications — automate in a language that approaches plain English, using SSH, with no agents to install on remote systems. https://docs.ansible.com/ansible/
aws2-wrap                 1.0.2      Simple script to export current AWS SSO credentials or run a sub-process with them
cloudflared               2019.9.0   Argo Tunnel client
ctop                      0.7.3      Top-like interface for container metrics
gitleaks                  4.1.0      Audit git repos for secrets 🔑
gomplate                  3.6.0      A flexible commandline tool for template rendering. Supports lots of local and remote datasources.
json2hcl                  0.0.6      Convert JSON to HCL, and vice versa
lab                       0.17.2     
molecule                  3.0.2      Molecule aids in the development and testing of Ansible roles
packer                    1.5.5      Packer is a tool for creating identical machine images for multiple platforms from a single source configuration.
shellcheck                0.7.0      ShellCheck, a static analysis tool for shell scripts
shfmt                     3.0.2      A shell parser, formatter and interpreter (POSIX/Bash/mksh)
sops                      3.5.0      Secrets management stinks, use some sops!
terraform                 0.12.24    Terraform is a tool for building, changing, and combining infrastructure safely and efficiently.
terraform-0.11            0.11.14    Terraform is a tool for building, changing, and combining infrastructure safely and efficiently.
terraform-docs            0.8.2      Generate docs from terraform modules
yamllint                  1.21.0     A linter for YAML files.
```
